import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Hannes Rosenkranz
 *
 */
public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieVersorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffname;
	// wir benutzen in diesem Falle die superclass List von Arraylist welche im
	// Konstruktor auf ArrayList gesetzt wird
	private List<String> broadcastKommunikator;
	private List<Ladung> ladungsverzeichnis;

	public Raumschiff() {

	}

	public Raumschiff(int photonentorpedoAnzahl, int energieVersorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffname) {
		super();
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieVersorgungInProzent = energieVersorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffname = schiffname;
		this.broadcastKommunikator = new ArrayList<String>();
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}

	/**
	 * Fuegt die uebergebende Ladung dem Ladungsverzeichnis hinzu.
	 *
	 * @param neueLadung
	 */
	public void addLadung(Ladung neueLadung) {
		for (Ladung ladung : ladungsverzeichnis) {
			if (ladung.getBezeichnung() == neueLadung.getBezeichnung()) {
				ladung.setMenge(ladung.getMenge() + neueLadung.getMenge());
				return;
			}
		}
		ladungsverzeichnis.add(neueLadung);
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}


	public int getEnergieVersorgungInProzent() {
		return energieVersorgungInProzent;
	}


	public void setEnergieVersorgungInProzent(int energieVersorgungInProzent) {
		this.energieVersorgungInProzent = energieVersorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}


	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}


	public int getHuelleInProzent() {
		return huelleInProzent;
	}


	public void setHuelleInProzent(int hülleInProzent) {
		this.huelleInProzent = hülleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffname() {
		return schiffname;
	}

	public void setSchiffname(String schiffname) {
		this.schiffname = schiffname;
	}

	@Override
	public String toString() {
		return "photonentorpedoAnzahl=" + photonentorpedoAnzahl + "\nenergieVersorgungInProzent="
				+ energieVersorgungInProzent + "\nschildeInProzent=" + schildeInProzent + "\nhülleInProzent="
				+ huelleInProzent + "\nlebenserhaltungssystemeInProzent=" + lebenserhaltungssystemeInProzent
				+ "\nandroidenAnzahl=" + androidenAnzahl + "\nschiffname=" + schiffname + "\nbroadcastKommunikator="
				+ String.join(", ", broadcastKommunikator) + "\nladungsverzeichnis:" + ladungsverzeichnisToString();
	}

	/**
	 * Hilfsmethode fuer toString, gibt den Inhalt der Liste des
	 * Ladungsverzeichnisses als String zureck
	 *
	 * @return Inhalt des Ladungsverzeichnisses
	 */
	private String ladungsverzeichnisToString() {
		String ladungsOutput = "";
		for (Ladung ladung : ladungsverzeichnis) {
			ladungsOutput += ladung.toString() + " ";
		}

		return ladungsOutput.trim();
	}

	/*
	 * Gibt den Inhalt des Ladungsverzeichnisses auf der Console aus
	 */
	public void ladungsverzeichnisAusgeben() {
		System.out.println("Ladungsverzeichnis: " + ladungsverzeichnisToString());
	}

	/**
	 * Schiesst einen Photonentorpedo auf das Ziel Raumschiff, wenn die Anzahl 0 ist
	 * wird -=*Click*=- an alle ausgegeben, sonst wird die Anzahl reduziert und ein
	 * treffer beim Zielraumschiff eingetragen
	 *
	 * @param zielRaumschiff
	 */
	public void photonentorpedoSchiessen(Raumschiff zielRaumschiff) {
		// Der Name "r" für Raumschiff wurde geaendert da dieser nicht genug aussagt
		if (photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-");
		} else if (photonentorpedoAnzahl > 0) {
			photonentorpedoAnzahl--;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(zielRaumschiff);
		}
	}

	/**
	 * Schiesst eine Phaserkanone auf das Zielraumschiff, ist die Energieversorgung
	 * unter 50 wird -=*Click*=- an alle ausgegeben, ist sie über 50 wird 50 von der
	 * Energieversorgung abgezogen und es wird ein Treffer beim Zielraumschiff
	 * notiert
	 *
	 * @param zielRaumschiff
	 */
	public void phaserkanoneSchiessen(Raumschiff zielRaumschiff) {
		if (energieVersorgungInProzent <= 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			energieVersorgungInProzent -= 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(zielRaumschiff);
		}
	}

	/**
	 * Notiert einen Treffer bei dem Zielraumschiff und zieht von den Schilden, der
	 * Energieversorgung und der Hülle 50% ab. Ausserdem wird an alle ausgegeben
	 * wenn die Lebenserhaltung zerstört wurde und dass das Schiff getroffen wurde
	 *
	 * @param zielRaumschiff
	 */
	private void treffer(Raumschiff zielRaumschiff) {
		zielRaumschiff.setSchildeInProzent(zielRaumschiff.getSchildeInProzent() - 50);

		if (zielRaumschiff.getSchildeInProzent() <= 0) {
			zielRaumschiff.setSchildeInProzent(0);
			zielRaumschiff.setEnergieVersorgungInProzent(zielRaumschiff.getEnergieVersorgungInProzent() - 50);
			zielRaumschiff.setHuelleInProzent(zielRaumschiff.getHuelleInProzent() - 50);

			if (zielRaumschiff.getHuelleInProzent() <= 0 && zielRaumschiff.getEnergieVersorgungInProzent() <= 0) {
				zielRaumschiff.setHuelleInProzent(0);
				zielRaumschiff.setEnergieVersorgungInProzent(0);

				zielRaumschiff.setLebenserhaltungssystemeInProzent(0);
				nachrichtAnAlle("Die Lebenserhaltungssysteme von " + zielRaumschiff.getSchiffname()
						+ " wurden vollständig vernichtet!");
			}
		}

		nachrichtAnAlle(zielRaumschiff.getSchiffname() + " wurde getroffen!");
	}

	/**
	 * Gibt die Liste des Broadcastkommikators zurueck
	 *
	 * @return broadcastKommunikator
	 */
	public List<String> eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikator;
	}

	/**
	 * Die message wird dem Broadcastkommunikator hinzugefuegt
	 *
	 * @param message
	 */
	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message);
	}

	/**
	 * Laed die uebergebende Torpedoanzahl aus dem Ladungsverzeichnis in das
	 * Raumschiff und erhoeht die photonentorpedoAnzahl
	 *
	 * @param anzahlTorpedos die aus dem Ladungsverzeichnis ins Schiff geladen
	 *                       werden
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {

		for (Ladung ladung : ladungsverzeichnis) {
			if (ladung.getBezeichnung().equals("Photonentorpedo")) {
				if (anzahlTorpedos >= ladung.getMenge()) {
					photonentorpedoAnzahl += ladung.getMenge();
					System.out.println("[" + ladung.getMenge() + "] Photonentorpedo(s) eingesetzt");
					ladung.setMenge(0);
					return;
				} else {
					ladung.setMenge(ladung.getMenge() - anzahlTorpedos);
					System.out.println("[" + anzahlTorpedos + "] Photonentorpedo(s) eingesetzt");
					return;
				}
			}
		}

		System.out.println("Keine Photonentorpedos gefunden!");
		nachrichtAnAlle("-=*Click*=-");
	}

	/**
	 * Entfernt die Ladungen aus dem Ladungsverzeichnis die eine Menge von 0 hat
	 */
	public void ladungsverzeichnisAufraeumen() {
		Iterator<Ladung> ladungsIterator = ladungsverzeichnis.iterator();

		while (ladungsIterator.hasNext()) {
			Ladung ladung = ladungsIterator.next();
			if (ladung.getMenge() <= 0) {
				ladungsIterator.remove();
			}
		}

	}

	/**
	 * Fuehrt anhand der uebergebnden Schiffstrukturen eine Reparatur am Raumschiff
	 * durch, ist die Androiden anzahl 0 kann keine Reparatur durchgefuert werden.
	 * Der Reparaturwert wird mittels Zuffalsszahlen ermittelt.
	 *
	 * @param schutzschilde
	 * @param energieversorgung
	 * @param schiffshuelle
	 * @param anzahlDroiden     Droiden die eingesetzt werden
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {

		Random random = new Random();
		int randomNumber = random.nextInt(101);
		int eingesetzteAndroiden;
		int reparaturWert;

		if (anzahlDroiden >= androidenAnzahl) {
			eingesetzteAndroiden = androidenAnzahl;
		} else {
			eingesetzteAndroiden = anzahlDroiden;
		}

		reparaturWert = (randomNumber * eingesetzteAndroiden)
				/ getAnzahlAusgewaehlteSchiffsstruktur(schutzschilde, energieversorgung, schiffshuelle);

		if (schutzschilde) {
			schildeInProzent += reparaturWert;
		}

		if (energieversorgung) {
			energieVersorgungInProzent += reparaturWert;
		}

		if (schiffshuelle) {
			huelleInProzent += reparaturWert;
		}

	}

	/**
	 * Hilfsmethode zur errechnung der Anzahl der ausgewaehlten Schiffsstrukturen
	 *
	 * @param schutzschilde
	 * @param energieversorgung
	 * @param schiffshuelle
	 * @return
	 */
	public int getAnzahlAusgewaehlteSchiffsstruktur(boolean schutzschilde, boolean energieversorgung,
			boolean schiffshuelle) {
		int schiffsstrukturAnzahl = 0;

		if (schutzschilde) {
			schiffsstrukturAnzahl++;
		}

		if (energieversorgung) {
			schiffsstrukturAnzahl++;
		}

		if (schiffshuelle) {
			schiffsstrukturAnzahl++;
		}

		return schiffsstrukturAnzahl;
	}

}
